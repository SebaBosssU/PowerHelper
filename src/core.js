define('two/compariser', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var PowerHelper = {}
    PowerHelper.version = '__compariser_version'
    PowerHelper.init = function () {
        Locale.create('compariser', __compariser_locale, 'pl')

        PowerHelper.initialized = true
    }
    PowerHelper.run = function () {
        if (!PowerHelper.interfaceInitialized) {
            throw new Error('PowerHelper interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return PowerHelper
})