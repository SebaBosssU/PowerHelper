require([
    'two/ready',
    'two/compariser',
    'two/compariser/ui'
], function (
    ready,
    PowerHelper
) {
    if (PowerHelper.initialized) {
        return false
    }

    ready(function () {
        PowerHelper.init()
        PowerHelper.interface()
        PowerHelper.run()
    })
})
