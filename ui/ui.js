define('two/compariser/ui', ['two/compariser', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(PowerHelper, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var bindEvents = function() {
    }
    var PowerInterface = function() {
        ui = new Interface('PowerHelper', {
            activeTab: 'compariser',
            template: '__compariser_html_window',
            css: '__compariser_css_style',
            replaces: {
                locale: Locale,
                version: PowerHelper.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('compariser', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        bindEvents()
        PowerHelper.interfaceInitialized = true
        return ui
    }
    PowerHelper.interface = function() {
        PowerHelper.interface = PowerInterface()
    }
})